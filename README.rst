fmconcert
=========

|Pypi|

Download France Musique concert file by scrapping on broadcast webpage

Installation
------------

Install pip
~~~~~~~~~~~

.. code:: bash

    curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py
    python /tmp/get-pip.py --user

Install fmconcert with pip
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    pip install --user fmconcert

Usage
-----

.. code:: bash

    fmconcert https://www.francemusique.fr/emissions/...[end_of_url]

.. |Pypi| image:: https://img.shields.io/pypi/v/fmconcert.svg
   :target: https://pypi.python.org/pypi/fmconcert
